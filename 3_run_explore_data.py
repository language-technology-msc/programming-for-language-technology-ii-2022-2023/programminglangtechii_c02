# -*- coding: utf-8 -*-
"""
Created on Sun Apr  4 16:37:20 2021

@author: user
"""

import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np

# %% load prepared data
df = pd.read_pickle('prepared_dataframe.pickle')

# %% get a female and a male actor and compare

act01 = df[ df['actor_ID'] == '01' ]
act02 = df[ df['actor_ID'] == '02' ]
folder_for_figs = 'act_01_02'

# %% or another pair
act01 = df[ df['actor_ID'] == '03' ]
act02 = df[ df['actor_ID'] == '04' ]
folder_for_figs = 'act_03_04'

# %% here you can test other splits, e.g. male or female.
# You can also add information about emotion in audio_file_representation, 
# e.g., calm vs angry, and test out such categorisation tasks. For this,
# you will need to add the required information in 
# 1_run_save_data_representation.py


# %% make some graphs for intuitive understanding

plt.boxplot( [ act01['mean_centroid'] , act02['mean_centroid'] ] )
plt.savefig('figs/' + folder_for_figs + '/mean_centroids.png', dpi=300)

plt.clf()
plt.boxplot( [ act01['std_centroid'] , act02['std_centroid'] ] )
plt.savefig('figs/' + folder_for_figs + '/std_centroids.png', dpi=300)

plt.clf()
plt.boxplot( [ act01['mean_bandwidth'] , act02['mean_bandwidth'] ] )
plt.savefig('figs/' + folder_for_figs + '/mean_bandwidths.png', dpi=300)

plt.clf()
plt.boxplot( [ act01['std_bandwidth'] , act02['std_bandwidth'] ] )
plt.savefig('figs/' + folder_for_figs + '/std_bandwidths.png', dpi=300)

plt.clf()
plt.plot( act01['mean_centroid'] , act01['std_centroid'] , 'bx', alpha=0.8 )
plt.plot( act02['mean_centroid'] , act02['std_centroid'] , 'r+', alpha=0.8 )
plt.savefig('figs/' + folder_for_figs + '/mean_std_centroids.png', dpi=300)

plt.clf()
plt.plot( act01['mean_bandwidth'] , act01['std_bandwidth'] , 'bx', alpha=0.8 )
plt.plot( act02['mean_bandwidth'] , act02['std_bandwidth'] , 'r+', alpha=0.8 )
plt.savefig('figs/' + folder_for_figs + '/mean_std_bandwidths.png', dpi=300)

# %% apply and plot with PCA

act01_features = act01.loc[:,act01.columns[2:]].to_numpy()
act02_features = act02.loc[:,act02.columns[2:]].to_numpy()
all_features = np.vstack((act01_features, act02_features))

pca = PCA(n_components=2)
pca_features = np.vstack( all_features )
all_pca = pca.fit_transform( np.vstack( pca_features ) )

plt.clf()
plt.plot( all_pca[:act01_features.shape[0], 0] , all_pca[:act01_features.shape[0], 1] , 'bx', alpha=0.8 )
plt.plot( all_pca[act01_features.shape[0]+1:, 0] , all_pca[act01_features.shape[0]+1:, 1] , 'r+', alpha=0.8 )
plt.savefig('figs/' + folder_for_figs + '/pca.png', dpi=300)

# %%
'''
Exercises:
1) Analyse data on the basis of male-female comparison.

2) Analyse data on the basis of calm-angry comparison.
'''